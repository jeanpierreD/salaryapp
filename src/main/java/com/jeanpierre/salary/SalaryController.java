package com.jeanpierre.salary;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.jeanpierre.job.JobRepository;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequestMapping(path="/salary")
public class SalaryController {
  
    @Autowired
    private SalaryRepository salaryRepository;

    @Autowired
    private JobRepository jobRepository;

    @GetMapping(path="/list")
    public String list(Model model)
    {
        model.addAttribute("salaries", salaryRepository.findAll());
        return "salary/list";
    }

    @GetMapping(path="/new")
    public String newSalaryForm(Model model)
    {
        model.addAttribute("salary", new Salary());
        model.addAttribute("jobs", jobRepository.findAll());
        return "salary/new";
    }

    @PostMapping(path="/new")
    public String saveSalary(@Valid Salary salary, BindingResult result, Model model, RedirectAttributes redirectAttributes){
        if(result.hasErrors()) {
            model.addAttribute("jobs", jobRepository.findAll());
            redirectAttributes.addFlashAttribute("resultError", result);
            return "redirect:/salary/new";
        }

        salaryRepository.save(salary);

        redirectAttributes.addFlashAttribute("message", "A salary has been added.");

        return "redirect:/salary/list";
    }

    @GetMapping(path="/{id}/edit")
    public String edit(@PathVariable Integer id, Model model) 
    {
        Salary s = salaryRepository.findById(id).get();
        model.addAttribute("salary", s);
        model.addAttribute("jobs", jobRepository.findAll());
        return "salary/edit";
    }

    @PostMapping(path="/{id}/update")
    public String updateSalary(@Valid Salary salary, BindingResult result, @PathVariable Integer id, RedirectAttributes redirectAttributes)
    {
        if(result.hasErrors()) {
            redirectAttributes.addFlashAttribute("resultError", result);
            return "redirect:/salary/" + id + "/edit";
        }

        Salary s = salaryRepository.findById(id).get();
        s.setFirstname(salary.getFirstname());
        s.setLastname(salary.getLastname());
        s.setJob(salary.getJob());

        salaryRepository.save(s);

        redirectAttributes.addFlashAttribute("message", "A salary has been updated.");

        return "redirect:/salary/list";
    }

    @PostMapping(path="/{id}/delete")
    public String deleteSalary(@PathVariable Integer id, RedirectAttributes redirectAttributes)
    {
        salaryRepository.deleteById(id);

        redirectAttributes.addFlashAttribute("message", "A salary has been deleted.");

        return "redirect:/salary/list";
    }
}