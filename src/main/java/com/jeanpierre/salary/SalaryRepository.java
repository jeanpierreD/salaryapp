package com.jeanpierre.salary;

import org.springframework.data.repository.CrudRepository;

public interface SalaryRepository extends CrudRepository<Salary, Integer> {}