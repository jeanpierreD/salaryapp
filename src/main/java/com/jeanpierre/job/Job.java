package com.jeanpierre.job;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.jeanpierre.salary.Salary;
 
@Entity
public class Job {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message="* message is blank")
    private String name;

    @OneToMany(mappedBy = "job", cascade=CascadeType.ALL, orphanRemoval = true)
    private List<Salary> salaries;
    
    public void setId(Integer id) {
        this.id = id;
    }

    public List<Salary> getSalaries() {
        return this.salaries;
    }

    public void setSalaries(List<Salary> salaries) {
        this.salaries = salaries;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}