package com.jeanpierre.job;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequestMapping(path="/job")
public class JobController implements WebMvcConfigurer {
  
    @Autowired
    private JobRepository jobRepository;

    @GetMapping(path="/list")
    public String list(Model model)
    {
        model.addAttribute("jobs", jobRepository.findAll());
        return "job/list";
    }

    @GetMapping(path="/new")
    public String newJobForm(Job job)
    {
        return "job/new";
    }

    @PostMapping(path="/new")
    public String saveJob(@Valid Job job, BindingResult result, Model model, RedirectAttributes redirectAttributes)
    {
        if(result.hasErrors()) {
            redirectAttributes.addFlashAttribute("resultError", result);
            return "redirect:/job/new";
        }

        jobRepository.save(job);

        redirectAttributes.addFlashAttribute("message", "A job has been added.");

        return "redirect:/job/list";
    }

    @GetMapping(path="/{id}/edit")
    public String edit(@PathVariable Integer id, Model model) 
    {
        Job j = jobRepository.findById(id).get();
        model.addAttribute("job", j);
        return "job/edit";
    }

    @PostMapping(path="/{id}/update")
    public String updateJob(@Valid Job job, BindingResult result, @PathVariable Integer id, RedirectAttributes redirectAttributes)
    {
        if(result.hasErrors()) {
            redirectAttributes.addFlashAttribute("resultError", result);
            return "redirect:/job/ " + id + "/edit";
        }

        Job j = jobRepository.findById(id).get();
        j.setName(job.getName());

        jobRepository.save(j);

        redirectAttributes.addFlashAttribute("message", "A job has been updated.");

        return "redirect:/job/list";
    }

    @PostMapping(path="/{id}/delete")
    public String deleteJob(@PathVariable Integer id, RedirectAttributes redirectAttributes)
    {
        Job job = jobRepository.findById(id).get();

        if(job.getSalaries().size() == 0) {
            jobRepository.delete(job);
            redirectAttributes.addFlashAttribute("message", "A job has been deleted.");
        }

        return "redirect:/job/list";
    }
}